// All links-related publications
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

import {shuffledCards,sortCards,cardsToBase64,valueCards64,pointsOfPlayer,pointsOfTeam} from '/imports/game.js';
import { Card } from '/imports/card.js';
import { showoff } from '/imports/showoff.js';

export function add_player(userId,gameId){

  let playerId = Players.insert({
    userId,
    gameId,
    teamNr: 0,
    points: 0,
    createdAt: new Date()
  });

  let game = Games.findOne(gameId);

  game.userIds.push(userId);
  game.playerIds.push(playerId);

  Games.update(gameId,game);

  return playerId;
}

export function remove_player(userId,gameId){

  let player = Players.findOne({
    userId,
    gameId
  });

  let game = Games.findOne(gameId);

  if(player && game && game.rounds.length == 0){ // do not delete if game is running
    game.userIds = game.userIds.filter((id)=>id != userId);
    game.playerIds = game.playerIds.filter((id)=>id != player._id);

    if(game.userIds.length > 0){
      Games.update(gameId,game);
    }else{
      Games.remove(gameId);
    }

    Players.remove(player._id);

    return player._id;
  }
}

export function new_round(game){

  let gameId = game._id;
  let stack = shuffledCards();
  let numCards = stack.length;

  Players.find({gameId}).forEach((player)=>{
    let cards = [];

    // give every player equal ammount of cards. We need to round for 5 players
    let cardsPerPlayer = Math.round(numCards / game.nrPlayers);
    if(game.dapp){
      // one Card per player goes to the Dapp
      cardsPerPlayer--;
    }

    for(let c = 0; c < cardsPerPlayer; c++){
      cards.push(stack.pop());
    }
    cards = sortCards(cards);

    Players.update(player._id,{$set:{
      cards: cardsToBase64(cards),
      stitches: [],
      showoff: false
    },$unset:{
      lastStitch: 1
    }})
  });

  let leadposition = (2+game.rounds.length) % (game.nrPlayers) + 1;
  let leadPlayer = Players.findOne({gameId,position:leadposition});

  while(leadPlayer.userId == "bot"){
    leadposition = leadposition + 1 % (game.nrPlayers) + 1;
    leadPlayer = Players.findOne({gameId,position:leadposition});
  }

  // all the cards left are the dapp
  return {
    step: "bet",
    dapp: game.dapp ? cardsToBase64(sortCards(stack)) : [],
    bid: 100,
    outbid: game.bot ? ["bot"] : [],
    leader: leadPlayer.userId,
    currentPlayer: (game.rounds.length % game.nrPlayers) + 1,
    stack: []
  }
}

export function evalRound(gameId,round){
  let leader = Players.findOne({gameId,userId:round.leader});

  if(round.durch){
    round.won = pointsOfPlayer(leader) == 250; // 240 stiches + last stich + 0 showoff = 250
  }else{
    let leaderTeam = Players.find({gameId,teamNr:leader.teamNr});
    round.won = pointsOfTeam(leaderTeam) >= round.bid;
  }


  Players.find({gameId}).forEach((player) => {
    let points = 0;

    let teamStiches = 0;
    Players.find({gameId,teamNr:player.teamNr}).forEach((p) => {
      teamStiches += p.stitches.length;
    });

    if(teamStiches){
      points += valueCards64(player.stitches);
      points += player.showoff.points;
      points += (player.lastStitch ? 10 : 0);
    }

    if(!round.won && player.teamNr == leader.teamNr){
      if(player._id == leader._id) {
        points = round.bid * (round.giveUp ? -1 : -2);
      }else{
        points = 0;
      }
    }

    Players.update(player._id,{$set:{
      points: player.points + points
    }});

  });

  return round;
}

export function is_user_in_game(userId, game){
  return userId && game && game.userIds.includes(userId);
}

// return true if everyPlayer is waiting for next step
export function waitForAllPlayers(userId,game){

  if(game){

    let waiting = game.waiting ? game.waiting : [];

    if(!waiting.includes(userId)){
      waiting.push(userId);
      Games.update(game._id,{$set:{
        waiting
      }})
    }
    if(waiting.length == game.nrPlayers){
      Meteor.setTimeout(()=>{
        Games.update(game._id,{$unset:{
          waiting:1
        }})
      },500);
      return true;
    }

  }
  return false;
}
