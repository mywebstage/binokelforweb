// All links-related publications
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

import {shuffledCards,sortCards,sortCards64,validCard64,cardsToBase64,cardsFromBase64,winnerCard64,valueCards64,pointsOfPlayer,pointsOfTeam} from '/imports/game.js';
import {new_round,evalRound,waitForAllPlayers} from './functions.js';

import { Card } from '/imports/card.js';
import { showoff } from '/imports/showoff.js';


// Plays every 3 to 5 Seconds
Meteor.setInterval(()=>{

  Games.find({
    "bot": true,
    "rounds.0": {$exists: true},
    "lastMove": {$lte:new Date(new Date() - 3000)}
  }).forEach((game) => {

    let bot = Players.findOne({userId:"bot",gameId:game._id});
    let roundChange = false;
    let round = game.rounds.pop();

    if(round.step == "bet"){
      // nothing to do
    }

    if(round.step == "exclaim" && round.exclaim && round.exclaim.victim == "bot"){
      round.step = "trump";
      roundChange = true;
    }

    if(round.step == "showoff"){
      if(!bot.showoff){
        Players.update(bot._id,{$set:{
          showoff: showoff(cardsFromBase64(bot.cards),round.trump)
        }});
        roundChange = true;
      }
      waitForAllPlayers("bot", game);
    }

    if(round.step == "playing" && round.winnerCard && round.winnerCard.playerId == bot._id){
      round.stack = [];
      delete round.winnerCard;
      if(!bot.cards.length){
        round.step = "summary";
      }
      roundChange = true;
    }

    if(round.step == "playing" && !round.winnerCard && round.currentPlayer == bot.position){

      let cards = validCard64(round.stack, bot.cards, round.trump);
      cards = sortCards64(cards,false);
      let cardIndex = 0;

      round.stack.push({card: cards[cardIndex], playerId: bot._id});

      if(winnerCard64(round.stack, round.trump).playerId != bot._id){
        cardIndex = cards.length - 1;
        round.stack.pop();
        round.stack.push({card: cards[cardIndex], playerId: bot._id})
      }

      delete bot.cards[bot.cards.indexOf(cards[cardIndex])];

      Players.update(bot._id,{$set:{
        cards: bot.cards.filter((e)=>e)
      }});

      if(round.currentPlayer < game.nrPlayers){
        round.currentPlayer++;
      }else{
        round.currentPlayer = 1;
      }

      if(round.stack.length == game.nrPlayers){
        round.winnerCard = winnerCard64(round.stack, round.trump);

        let winner = Players.findOne(round.winnerCard.playerId);
        round.stack.forEach((stackCard)=>{
          winner.stitches.push(stackCard.card);
        });

        if(winner.cards.length == 0){ // round over
          Players.update(winner._id,{$set:{
            stitches: winner.stitches,
            lastStitch: true
          }});

          round = evalRound(game._id,round);

        }else{
          Players.update(winner._id,{$set:{
            stitches: winner.stitches
          }});

          // Winner plays next
          round.currentPlayer = winner.position;
        }
      }
      roundChange = true;
    }

    if(round.step == "summary" && waitForAllPlayers("bot",game)){
      game.rounds.push(round);
      round = new_round(game);
      roundChange = true;
    }

    if(roundChange){
      game.rounds.push(round);
      Games.update(game._id,{ $set: {
        rounds: game.rounds,
        lastMove: new Date()
      } });
    }else{
      Games.update({
        _id:game._id,
        "lastMove":{$lte:new Date(new Date() - 3000)}
      },{ $unset: {
        lastMove: 1
      } });
    }

  });
},2000);
