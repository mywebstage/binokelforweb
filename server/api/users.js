// All links-related publications
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Games } from '../../imports/collections/games.js';
import { Players } from '../../imports/collections/players.js';


Meteor.publish('self', ()=>{
  Meteor.users.update(Meteor.userId(),{$set:{
    lastLogin: new Date()
  }});
  return Meteor.users.find(Meteor.userId(),{
    fields: {
      friends: 1
    }
  });
});

Meteor.publish('friends', ()=>{
  let user = Meteor.user();
  if(user){
    return Meteor.users.find({
        friends: user._id
      },{
        fields: {
          username: 1
        }
    });
  }
});

Meteor.publish('friendRequests', (friends)=>{
  let user = Meteor.user();
  if(user){
    return Meteor.users.find({
      $or: [
        {_id: {$in: friends}},
        {friends: user._id },
      ]
    },{
      fields: {
        username: 1,
        friends: 1
      }
    });
  }
});

Meteor.publish('users', (userIds)=>{
  let userId = Meteor.userId();
  if(userId){
    return Meteor.users.find({_id: {$in: userIds}},{
      fields: {
        username: 1
      }
    });
  }
});

function checkUsername(username,callback){
  username = username.trim();
  username = username.split(" ").join("_");
  if(!username){
    return "username_empty";
  }
  if(Meteor.users.findOne({username: username})){
    return "username_exists";
  }
  return callback(username);
}

// Delete Users after 90 Days inactivity
Meteor.setInterval(()=>{
  let deadline = new Date() - 1000*60*60*24*90;
  Meteor.users.find({lastLogin:{$lt: new Date(deadline)}},{fields:{}}).forEach((user) => {
    removeUserAndFriends(user._id);
  });
},60000);

Meteor.methods({
  'users.setUsername'(username){
    if(Meteor.userId()){
      check(username, String);
      return checkUsername(username,(checkedUsername)=>{
        return Meteor.users.update(Meteor.userId(),{ $set: {
          username: checkedUsername
        } });
      });
    }
  },
  'users.getUsernames'(username){
    let userIds = [];
    if(Meteor.userId()){
      check(username, String);
      Meteor.users.find({
        username: new RegExp(username, 'gi')
      },{
        fields:{username:1}
      }).forEach((user)=>userIds.push(user));
    }
    return userIds;
  },
  'users.addFriend'(username){
    let userId = Meteor.userId();
    if(userId){
      check(username, String);
      let friend = Meteor.users.findOne({username});
      if(friend && friend._id != userId){
        let friends = [userId];
        if(friend && friend.friends && friend.friends.length > 0){
          friend.friends.forEach((friendId)=>{
            if(!friends.includes(friendId)){
              friends.push(friendId);
            }
          })
        }
        Meteor.users.update(friend._id,{ $set: {
          friends
        } });
        return {username: friend.username};
      }
    }
  },
  'users.removeFriend'(friendId){
    let userId = Meteor.userId();
    if(userId){
      check(friendId, String);
      let friend = Meteor.users.findOne(friendId);
      let user = Meteor.user();
      if(friend && friend.friends){
        let friends = friend.friends.filter((friendId)=>friendId != userId);
        Meteor.users.update(friend._id,{ $set: {
          friends
        } });
      }
    }
  },
  'users.delete'(){
    let userId = Meteor.userId();
    if(userId){
      Meteor.setTimeout(()=>{
        // delete Games where already has a bot, players in this game will be deleted within 14 days
        Games.remove({userIds:userId,bot:true});

        // Set all players of user to bots
        Players.update({userId},{$set:{
          userId: "bot"
        }});

        // change game to a bot-game
        Games.find({userIds:userId,bot:false}).forEach((game) => {
          let userIds = game.userIds.map(id => id != userId ? id : "bot");
          let round = game.rounds.pop();
          
          if(["showoff","playing","summary"].includes(round.step)){
            game.rounds.push(round);
          }
          Games.update(game._id,{$set:{
            userIds,
            bot:true,
            rounds: game.rounds,
            lastMove: new Date()
          }})
        });

        removeUserAndFriends(userId);
      },1000)
    }
  }
});

function removeUserAndFriends(userId){
  // delete users id on all friends
  Meteor.users.find({friends:userId},{fields:{friends:1}}).forEach((friend) => {
    Meteor.users.update(friend._id,{ $set: {
      friends: friend.friends.filter((friendId)=>friendId != userId)
    } });
  });
  Meteor.users.remove(userId);
}
