// All links-related publications
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Games } from '../../imports/collections/games.js';
import { Players } from '../../imports/collections/players.js';


Meteor.publish('my_players', ()=>{
  let userId = Meteor.userId();
  if(userId){
    return Players.find({userId});
  }
});

Meteor.publish('players', (playerIds)=>{
  if(Meteor.userId()){
    return Players.find({_id: {$in: playerIds}},{
        fields:{
          userId: 1,
          gameId: 1,
          teamNr: 1,
          position: 1,
          points: 1,
          showoff: 1,
          stitches: 1,
          lastStitch: 1
        }
    });
  }
});
