// All links-related publications
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

import {shuffledCards,sortCards,sortCards64,validCard64,cardsToBase64,cardsFromBase64,winnerCard64,valueCards64,pointsOfPlayer,pointsOfTeam} from '/imports/game.js';
import {add_player,remove_player,new_round,evalRound,is_user_in_game,waitForAllPlayers} from '../functions.js';
import { Card } from '/imports/card.js';
import { showoff } from '/imports/showoff.js';


/*
  publish Games of User or Public Games that not started jet
*/
Meteor.publish('games', ()=>{
  let user = Meteor.user();
  if(user){
    return Games.find({
      $or: [
        {userIds: {$elemMatch: { $eq: user._id } }},
        {$and: [
          {private: false},
          {rounds: {$size: 0}}
        ]}
      ]
    });
  }
  return [];
});

Meteor.publish('gamesOfFriends', (friends)=>{
  return Games.find({
    $and: [
      {userIds: {$elemMatch: { $in: friends } }},
      {rounds: {$size: 0}}
    ]
  });
});

// Delete Games after 14 Days
Meteor.setInterval(()=>{
  let deadline = new Date() - 1000*60*60*24*14;
  Games.remove({createdAt:{$lt: new Date(deadline)}});
  Players.remove({createdAt:{$lt: new Date(deadline)}});
},60000);

Meteor.methods({

  'games.create'(name, private, simpleMode, exclaim, dapp, nrTeams, nrPlayers, goal, bot){
    check(name, String);
    check(private, Boolean);
    check(simpleMode, Boolean);
    check(dapp, Boolean);
    check(exclaim, Boolean);
    check(nrTeams, Number);
    check(nrPlayers, Number);
    check(goal, Number);
    check(bot, Boolean);

    let userId = Meteor.userId();

    if(userId){

      if(nrPlayers == 5) dapp = true;
      if(nrPlayers == 8) dapp = false;

      let gameId = Games.insert({
        name: "", // will be set on update few lines later...
        private,
        simpleMode,
        dapp,
        exclaim,
        bot,
        nrTeams,
        nrPlayers,
        goal,
        rounds: [],
        userIds: [],
        playerIds: [],
        createdAt: new Date()
      });

      if(!name){ // generate name if none is set
        name = "Spiel " + gameId.substr(0,4);
      }

      Games.update(gameId,{ $set: {
        name
      } });

      let playerId = add_player(userId,gameId,1);

      if(bot){
        Players.update(playerId,{$set:{
          teamNr: 2
        }})
        let botId = add_player("bot",gameId);
        Players.update(botId,{$set:{
          teamNr: 1
        }})
      }else{
        Players.update(playerId,{$set:{
          teamNr: 1
        }})
      }

      return {gameId, playerId};
    }
  },
  'games.changeTeam'(gameId,teamNr){
    check(gameId, String);
    check(teamNr, Number);

    let game = Games.findOne(gameId);
    let userId = Meteor.userId();
    let player = Players.findOne({userId,gameId});
    let numPlayer_in_game = Players.find({gameId}).count();
    let numPlayer_in_team = Players.find({teamNr,gameId}).count();
    let perTeam = game.nrPlayers / game.nrTeams;

    // if selected team does not exist
    teamNr = Math.floor(teamNr);
    if(teamNr > game.nrTeams || teamNr <= 0) return;


    // if user is not a player of the game: join if possible
    if(!player && game.nrPlayers > numPlayer_in_game){
      var playerId = add_player(userId,gameId);
    }else if(player && player._id){
      var playerId = player._id;
    }else{
      return;
    }

    if(perTeam > numPlayer_in_team){ // if team is full: exchange
      Players.update(playerId,{ $set: {
        teamNr,
      } });
    }else{
      //exchange
    }

  },
  'games.start'(gameId){
    check(gameId, String);
    let game = Games.findOne(gameId);

    let open_slots = 0;

    let perTeam = game.nrPlayers / game.nrTeams;

    for(let teamNr = 1; teamNr <= game.nrTeams; teamNr++){
      let players_in_team = Players.find({teamNr,gameId});
      if(players_in_team.count() == perTeam){

        // give position numbers
        //  1  |  2  |  3
        //  4  |  5  |  6

        // or

        // 1  |  2
        // 3  |  4


        let position = teamNr;
        players_in_team.forEach((player)=>{
          Players.update(player._id,{ $set: {
            position,
          } });
          position += game.nrTeams;
        });
      }else{
        let open_slots = game.nrPlayers - Players.find({$and: [
          {gameId},
          {teamNr: {$gte: 1}}
        ]}).count();

        return {open_slots};
      }
    }

    Players.remove({teamNr:0,gameId});

    Games.update(game._id,{ $set: {
      rounds: [new_round(game)],
      lastMove: new Date()
    } });

    return {};
  },
  'games.leave'(gameId){
    check(gameId, String);

    remove_player(Meteor.userId(),gameId);
  },
  'games.remove'(gameId){
    check(gameId, String);
    let userId = Meteor.userId();

    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game)){
      Players.remove({gameId});
      Games.remove(gameId);
    }

  },
  'games.bid'(gameId,bid){
    check(gameId, String);
    check(bid, Number);

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game) && game.rounds && game.rounds.length){
      let round = game.rounds.pop();
      if(round.step == "bet" && round.bid < bid){
        round.leader = userId;
        round.bid = bid;
        game.rounds.push(round);
        Games.update(gameId,{ $set: {
          rounds: game.rounds,
          lastMove: new Date()
        } });
      }
    }
  },
  'games.bid_out'(gameId){
    check(gameId, String);

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game)){
      let round = game.rounds.pop();

      if(round.step == "bet"){
        round.outbid.push(userId);
      }

      if(round.outbid.length == game.nrPlayers - 1){
        delete round.outbid;

        if(game.dapp){
          round.step = "dapp";

          // give leader the dapp-cards
          let player = Players.findOne({userId:round.leader, gameId});
          let cards = player.cards;
          round.dapp.forEach((card)=>{
            cards.push(card);
          })
          Players.update(player._id,{$set: {
            cards: sortCards64(player.cards)
          }})

        }else if(game.exclaim){
          round.step = "exclaim";
        }else{
          round.step = "trump";
        }

      }
      game.rounds.push(round);

      Games.update(gameId,{$set: {
        rounds: game.rounds
      }})
    }
  },
  'games.setDapp'(gameId,dappCards){
    check(gameId, String);
    check(dappCards, Array);

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game)){
      let round = game.rounds.pop();
      if(round.step == "dapp" && round.leader == userId && dappCards.length == round.dapp.length){

        let player = Players.findOne({userId,gameId});
        // move dappCards from players hand to his stitches
        let user_have_cards = true;
        dappCards.forEach((card) => {
          if(player.cards.includes(card)){
            delete player.cards[player.cards.indexOf(card)];
          }else{
            user_have_cards = false;
          }
        });

        if(!user_have_cards){
          return "You do not have the Cards.";
        }

        Players.update(player._id,{$set: {
          cards: player.cards.filter(Boolean),
          stitches: dappCards
        }})

        if(round.durch){
          round.step = "playing";
        }else if(game.exclaim){
          round.step = "exclaim";
        }else{
          round.step = "trump";
        }

        game.rounds.push(round);

        Games.update(gameId,{$set: {
          rounds: game.rounds
        }})
      }

    }
  },


  'games.exclaim'(gameId,exclaimBase64,returnBase64){
    check(gameId, String);
    check(exclaimBase64, String);
    check(returnBase64, String);

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game)){
      let round = game.rounds.pop();

      if(round.step == "exclaim" && round.leader == userId && !round.exclaim){
        let leader = Players.findOne({userId,gameId});
        let victims = [];
        Players.find({userId:{$ne:round.leader},gameId,cards:exclaimBase64}).forEach((victim) => {
          if(leader.position > victim.position){
            victim.position += game.nrPlayers;
          }
          victims.push(victim);
        });
        victims.sort((a,b)=>a.position-b.position);

        if(!victims[0]){
          return {error:"Kein Mitspieler hat diese Karte!"}
        }

        let c = victims[0].cards.indexOf(exclaimBase64);
        victims[0].cards[c] = returnBase64;

        let l = leader.cards.indexOf(returnBase64);
        leader.cards[l] = exclaimBase64;

        round.exclaim = {
          victim: victims[0].userId,
          card: exclaimBase64,
          return: returnBase64
        };

        Players.update(victims[0]._id,{$set:{
          cards: sortCards64(victims[0].cards)
        }})

        Players.update(leader._id,{$set: {
          cards: sortCards64(leader.cards)
        }})

        game.rounds.push(round);
        Games.update(gameId,{$set: {
          rounds: game.rounds,
          lastMove: new Date()
        }})
      }
    }
  },

  'games.acceptExclaim'(gameId){
    check(gameId, String);

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game)){
      let round = game.rounds.pop();
      if(round.step == "exclaim" && round.exclaim){
        round.step = "trump";

        game.rounds.push(round);

        Games.update(gameId,{$set: {
          rounds: game.rounds,
          lastMove: new Date()
        }})
      }
    }
  },

  // leader is setting trump
  // if leader gave up, we continue to the summary
  'games.setTrump'(gameId,trump){
    check(gameId, String);
    check(trump, String);

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game) && Card.colors.includes(trump)){
      let round = game.rounds.pop();
      if(round.step == "trump"){
        round.trump = trump;

        if(round.giveUp){
          let leader = Players.findOne({gameId,userId:round.leader});

          Players.update(leader._id,{$set:{
            stitches: [],
            showoff: showoff([], round.trump)
          }})

          Players.find({gameId,teamNr:{$ne: leader.teamNr}}).forEach((player) => {
            let stitches = [];
            // according to the rules get the others 10 Points per Player. We give a 10 Card to the stiches per player which will be counted later.
            let card = new Card("a","10");
            for(let i = 0; i < game.nrPlayers; i++){
              stitches.push(card.toBase64());
            }
            Players.update(player._id,{$set:{
              stitches,
              showoff: showoff(cardsFromBase64(player.cards), round.trump)
            }});
          });


          round = evalRound(gameId,round);
          round.step = "summary";
        }else{
          round.step = "showoff";
        }

        game.rounds.push(round);

        Games.update(gameId,{$set: {
          rounds: game.rounds,
          lastMove: new Date()
        }})

      }
    }
  },

  'games.setShowoff'(gameId,base64cards){
    check(gameId, String);
    check(base64cards, Array);

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);


    if(is_user_in_game(userId, game)){
      let round = game.rounds.pop();
      if(round.step == "showoff"){

        let cards = [];

        let player = Players.findOne({userId,gameId});
        for(key in base64cards){
          let card = new Card(base64cards[key]);
          if(!card.isValid() || !player.cards.includes(base64cards[key])){
            return "There is a unvalid Card";
          }else{
            cards.push(card);
          }
        }

        Players.update(player._id,{$set:{
          showoff: showoff(cards,round.trump)
        }});
      }
    }
  },

  'games.showoffReady'(gameId){
    check(gameId, String);

    let ready = true;
    Players.find({gameId},{fields:{
      showoff: 1
    }}).forEach((player)=>{
      ready = ready && player.showoff;
    });

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game) && waitForAllPlayers(userId, game) && ready){

      let round = game.rounds.pop();
      if(round.step == "showoff"){
        round.step = "playing";
      }
      game.rounds.push(round);

      Games.update(gameId,{$set: {
        rounds: game.rounds,
        lastMove: new Date()
      }})
    }
  },
  'games.playCard'(gameId, base64Card){
    check(gameId, String);
    check(base64Card, String);

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game)){

      let round = game.rounds.pop();
      let player = Players.findOne({userId,gameId});

      if( round.step == "playing"
          && round.currentPlayer == player.position
          && player.cards.includes(base64Card)
          && !round.winnerCard
          && validCard64(round.stack, player.cards, round.trump, base64Card)
        ){

        let cardIndex = player.cards.indexOf(base64Card);
        delete player.cards[cardIndex];

        Players.update(player._id,{$set:{
          cards: player.cards.filter((e)=>e)
        }});

        round.stack.push({card: base64Card, playerId: player._id});

        if(round.currentPlayer < game.nrPlayers){
          round.currentPlayer++;
        }else{
          round.currentPlayer = 1;
        }

        if(round.stack.length == game.nrPlayers){
          round.winnerCard = winnerCard64(round.stack, round.trump);

          let winner = Players.findOne(round.winnerCard.playerId);
          round.stack.forEach((stackCard)=>{
            winner.stitches.push(stackCard.card);
          });

          if(winner.cards.length == 0){ // round over
            Players.update(winner._id,{$set:{
              stitches: winner.stitches,
              lastStitch: true
            }});

            round = evalRound(gameId,round);

          }else{
            Players.update(winner._id,{$set:{
              stitches: winner.stitches
            }});

            // Winner plays next
            round.currentPlayer = winner.position;
          }
        }
        game.rounds.push(round);

        Games.update(gameId,{$set: {
          rounds: game.rounds,
          lastMove: new Date()
        }})
      }
    }
  },
  'games.nextStitch'(gameId){
    check(gameId, String);

    let game = Games.findOne(gameId);
    let userId = Meteor.userId();


    if(is_user_in_game(userId, game)){
      let round = game.rounds.pop();
      if(round.winnerCard && round.step == "playing"){
        round.stack = [];
        delete round.winnerCard;
        let player = Players.findOne({gameId});
        if(!player.cards.length){
          round.step = "summary";
        }
        game.rounds.push(round);
        Games.update(gameId,{$set: {
          rounds: game.rounds,
          lastMove: new Date()
        }})
      }
    }
  },
  'games.nextRound'(gameId){
    check(gameId, String);

    let game = Games.findOne(gameId);
    let round = game.rounds.pop();
    let userId = Meteor.userId();

    if(is_user_in_game(userId, game) && round.step == "summary" && waitForAllPlayers(userId,game)){

      game.rounds.push(round);
      game.rounds.push(new_round(game));

      Games.update(game._id,{ $set: {
        rounds: game.rounds,
        lastMove: new Date()
      } });
    }
  },

  'games.durch'(gameId){
    check(gameId, String);

    let userId = Meteor.userId();

    let game = Games.findOne(gameId);
    let round = game.rounds.pop();

    if(is_user_in_game(userId, game) && round.step != "playing" && round.leader == userId){

      let leader = Players.findOne({gameId,userId: round.leader});

      round.currentPlayer = leader.position; // Leader becomes first player
      round.durch = true;
      round.trump = false;
      round.bid = 1000;

      if(round.step == "showoff"){
        Players.update({gameId},{$set:{
          showoff: false
        }})
        round.step = "playing";
      }

      game.rounds.push(round);
      Games.update(gameId,{$set: {
        rounds: game.rounds,
        lastMove: new Date()
      }})
    }
  },

  'games.giveUp'(gameId){
    check(gameId, String);

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game)){
      let round = game.rounds.pop();

      if(round.step == "dapp" && round.leader == userId){

        round.giveUp = true;
        round.step = "trump";

        game.rounds.push(round);
        Games.update(gameId,{$set: {
          rounds: game.rounds,
          lastMove: new Date()
        }})
      }
    }
  },

  'games.end'(gameId){
    check(gameId, String);

    let userId = Meteor.userId();
    let game = Games.findOne(gameId);

    if(is_user_in_game(userId, game) && waitForAllPlayers(userId, game)){

      Games.remove(gameId);
      Players.remove({gameId});

    }
  }

});
