import chai from 'chai';
import { Card } from './card.js';
import {showoff} from '../imports/showoff.js'

let acorns = "a";
let leaves = "l";
let hearts = "h";
let bells = "b";

describe('showoff', function () {
  it('Rundgang', function () {

    let trump = bells;

    let cards = [
      new Card("a","K"),
      new Card("a","O"),
      new Card("l","K"),
      new Card("l","O"),
      new Card("h","K"),
      new Card("h","O"),
      new Card("b","K"),
      new Card("b","O")
    ]

    let result = showoff(cards, trump);

    chai.assert.equal(result.points, 240);
  })

  it('Rundgang + Family', function () {

    let trump = bells;

    let cards = [
      new Card("a","A"),
      new Card("a","10"),
      new Card("a","K"),
      new Card("a","O"),
      new Card("a","U"),
      new Card("l","K"),
      new Card("l","O"),
      new Card("h","K"),
      new Card("h","O"),
      new Card("b","K"),
      new Card("b","O")
    ]

    let result = showoff(cards, trump);

    chai.assert.equal(result.points, 320);
  })

  it('100 Ass', function () {

    let trump = bells;

    let cards = [
      new Card("a","A"),
      new Card("l","A"),
      new Card("h","A"),
      new Card("b","A"),

      new Card("h","A"),
      new Card("b","A")
    ]

    let result = showoff(cards, trump);

    chai.assert.equal(result.points, 100);
  })

  it('Binokel', function () {

    let trump = bells;

    let cards = [
      new Card("a","A"),
      new Card("l","O"),
      new Card("h","A"),
      new Card("b","U")
    ]

    let result = showoff(cards, trump);

    chai.assert.equal(result.points, 40);
  })

})
