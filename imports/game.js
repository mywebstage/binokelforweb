import {Card} from './card.js';

export const playerConstellations = [3,4,5,6,8];
export const playerTeamsConstellations = {
  3: [3],
  4: [2,4],
  5: [5],
  6: [2,3,6],
  8: [2,4,8]
};

export function winnerCard(cards, trump){
  let winnerCard = cards[0]; // first card is default winner

  cards.forEach((card)=>{
    if(card.isHigherThan(winnerCard,trump)){
      winnerCard = card;
    }
  })

  return winnerCard;
}

export function winnerCard64(stack, trump){
  // stack = [{card:base64Card,playerId},...]

  let cards = [];
  stack.forEach((player)=>{
    let card = new Card(player.card);
    card.playerId = player.playerId;
    cards.push(card);
  });

  return winnerCard(cards, trump);
}

export function shuffledCards(){
  let cards = [];

  let doubledColors = Card.colors.concat(Card.colors);
  doubledColors.forEach((color)=>{
    Card.symbols.forEach((symbol)=>{
      cards.push(new Card(color,symbol));
    })
  })

  cards.sort(() => Math.random()-0.5); // shuffle
  return cards;
}

export function shuffledCards64(){
  return cardsToBase64(shuffledCards());
}

export function sortCards(cards, withSymbol = true, trump = "a"){

  if(withSymbol){
    // order colors, that trump is the last array element
    let orderedColors = Card.colors.filter((color) => color != trump);
    orderedColors.push(trump);

    function cardValue(card){
      return orderedColors.indexOf(card.color) * 10 + Card.symbols.indexOf(card.symbol);
    }

    cards.sort((a,b)=>{
      return cardValue(b) - cardValue(a);
    })

  }else{
    cards.sort((a,b)=>{
      return b.value() - a.value();
    })
  }

  return cards;
}

export function sortCards64(base64Cards, withSymbol = true, trump = "a"){
  return cardsToBase64(sortCards(cardsFromBase64(base64Cards), withSymbol, trump));
}

export function validCard(openCards, handCards, trump, selectedCard = false){

  let filteredCards = handCards;

  if(openCards.length > 0){
    let sameColorCards = handCards.filter((card)=>openCards[0].color == card.color); // first Card gives color

    if(sameColorCards.length > 0){ // if player have the given color
      filteredCards = sameColorCards;
    }

    let bestCard = winnerCard(openCards, trump); // player have to play a better Card than previous player
    let betterCards = filteredCards.filter((card)=>card.isHigherThan(bestCard, trump));

    if(betterCards.length > 0){ // if user have better Cards
      filteredCards = betterCards;
    }
  }

  if(selectedCard){
    let matches = filteredCards.filter((card)=>card.isEqual(selectedCard));
    return matches.length > 0;
  }else{
    return filteredCards;
  }
}

export function validCard64(stack, handCards, trump, selectedCard = false){

  let openCards = [];
  stack.forEach((player) => {
    openCards.push(player.card);
  });

  openCards = cardsFromBase64(openCards);
  handCards = cardsFromBase64(handCards);

  if(selectedCard){
    selectedCard = new Card(selectedCard);
    return validCard(openCards, handCards, trump, selectedCard);
  }

  return cardsToBase64(validCard(openCards, handCards, trump));
}

export function valueCards(cards){
  let value = 0;
  cards.forEach((card) => {
    value += card.value() ? card.value() : 0;
  });
  return value;
}

export function valueCards64(base64Cards){
  return valueCards(cardsFromBase64(base64Cards));
}

export function pointsOfPlayer(player){
  let points = 0;
  points += valueCards64(player.stitches);
  points += (player.lastStitch ? 10 : 0);
  points += (player.showoff ? player.showoff.points : 0);
  return points;
}

export function pointsOfTeam(players){
  let points = 0;
  if(stichesOfTeam(players) > 0){ // Only if team have at least a stich
    players.forEach((player) => {
      points += pointsOfPlayer(player);
    });
  }
  return points;
}

export function stichesOfTeam(players){
  let stiches = 0;
  players.forEach((player) => {
    stiches += player.stitches.length;
  });
  return stiches;
}

export function cardsToBase64(cards){
  return cards.map((card)=>card.toBase64());
}

export function cardsFromBase64(base64Cards){
  return base64Cards.map((base64)=>new Card(base64));
}
