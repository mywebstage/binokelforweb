import chai from 'chai';
import { Card } from './card.js';
import {winnerCard, shuffledCards, sortCards, validCard, showoff, valueCards} from '../imports/game.js'

let acorns = "a";
let leaves = "l";
let hearts = "h";
let bells = "b";

describe('winnerCard', function () {
  it('A > 10', function () {
    let cards = [
      new Card(acorns,"7"),
      new Card(leaves,"A"),
      new Card(leaves,"10"),
      new Card(acorns,"K")
    ]
    chai.assert.equal(winnerCard(cards,leaves).toString(), "l-A");
  })
  it('first ist highest', function () {
    let cards = [
      new Card(acorns,"7"),
      new Card(leaves,"A"),
      new Card(leaves,"10"),
      new Card(hearts,"K")
    ]
    chai.assert.equal(winnerCard(cards,bells).toString(), "a-7");
  })
  it('same color, higher card', function () {
    let cards = [
      new Card(acorns,"O"),
      new Card(leaves,"A"),
      new Card(acorns,"10"),
      new Card(hearts,"K")
    ]
    chai.assert.equal(winnerCard(cards,bells).toString(), "a-10");
  })
  it('trump', function () {
    let cards = [
      new Card(acorns,"O"),
      new Card(leaves,"U"),
      new Card(acorns,"10"),
      new Card(hearts,"K")
    ]
    chai.assert.equal(winnerCard(cards,leaves).toString(), "l-U");
  })
  it('second looses', function () {
    let cards = [
      new Card(acorns,"U"),
      new Card(bells,"10"),
      new Card(leaves,"A"),
      new Card(bells,"10")
    ]
    chai.assert.equal(winnerCard(cards,bells).toString(), "b-10");
  })
})

describe('shuffledCards', function () {
  it('48 cards', function () {
    chai.assert.equal(shuffledCards().length, 48);
  })

  it('12 cards per color', function () {
    let cards = shuffledCards();

    Card.colors.forEach((color)=>{
      let cardsOfColor = cards.filter((card) => card.color == color);
      chai.assert.equal(cardsOfColor.length, 12);
    })
  })

  it('4 cards per symbol', function () {
    let cards = shuffledCards();

    Card.symbols.forEach((symbol)=>{
      let cardsOfSymbol = cards.filter((card) => card.symbol == symbol);
      chai.assert.equal(cardsOfSymbol.length, 8);
    })
  })

  it('different', function () {
    let cards = shuffledCards();
    let cards2 = shuffledCards();

    chai.assert.notEqual(JSON.stringify(cards), JSON.stringify(cards2));
  })
})


describe('sortCards', function () {
  it('sort colors', function () {
    let cards = [
      new Card(acorns,"O"),
      new Card(leaves,"K"),
      new Card(bells,"K"),
      new Card(hearts,"O")
    ]

    let sorted = sortCards(cards);
    chai.assert.equal(sorted[0].color, acorns);
    chai.assert.equal(sorted[1].color, leaves);
    chai.assert.equal(sorted[2].color, hearts);
    chai.assert.equal(sorted[3].color, bells);
  })

  it('sort symbols', function () {
    let cards = [
      new Card(bells,"O"),
      new Card(bells,"K"),
      new Card(bells,"A"),
      new Card(bells,"U")
    ]

    let sorted = sortCards(cards);
    chai.assert.equal(sorted[0].symbol, "A");
    chai.assert.equal(sorted[1].symbol, "K");
    chai.assert.equal(sorted[2].symbol, "O");
    chai.assert.equal(sorted[3].symbol, "U");
  })

  it('sort trump', function () {
    let cards = [
      new Card(bells,"U"),
      new Card(bells,"O"),
      new Card(leaves,"K"),
      new Card(hearts,"10")
    ]

    let sorted = sortCards(cards,leaves);
    chai.assert.equal(sorted[0].symbol, "K");
    chai.assert.equal(sorted[1].symbol, "10");
    chai.assert.equal(sorted[2].symbol, "O");
    chai.assert.equal(sorted[3].symbol, "U");
  })
})


describe('validCard', function () {
  it('first Card', function () {
    let trump = bells;

    let openCards = [];

    let handCards = [
      new Card(bells,"A"),
      new Card(acorns,"7"),
      new Card(bells,"10")
    ]

    chai.assert.equal(validCard(openCards, handCards, trump).length, 3);
  })

  it('allowed Cards', function () {
    let trump = bells;

    let openCards = [
      new Card(acorns,"U"),
      new Card(acorns,"O")
    ];

    let handCards = [
      new Card(acorns,"K"),
      new Card(leaves,"7"),
      new Card(acorns,"U"),
      new Card(acorns,"10")
    ]

    chai.assert.equal(validCard(openCards, handCards, trump).length, 2);
  })

  it('trump Card', function () {
    let trump = bells;

    let openCards = [
      new Card(acorns,"U"),
      new Card(acorns,"O")
    ];

    let handCards = [
      new Card(leaves,"K"),
      new Card(leaves,"7"),
      new Card(bells,"10")
    ]

    chai.assert.equal(validCard(openCards, handCards, trump, handCards[2]), true, "Play Trump");
    chai.assert.equal(validCard(openCards, handCards, trump, handCards[0]), false, "Having Trump");
  })

  it('lower Card', function () {
    let trump = bells;

    let openCards = [
      new Card(acorns,"O"),
      new Card(bells,"O")
    ];

    let handCards = [
      new Card(acorns,"K"),
      new Card(leaves,"7"),
      new Card(bells,"U"),
      new Card(acorns,"A"),
      new Card(acorns,"7")
    ]

    chai.assert.equal(validCard(openCards, handCards, trump).length, 3);
  })

  it('forbidden Cards', function () {
    let trump = bells;

    let openCards = [
      new Card(acorns,"U"),
      new Card(acorns,"O")
    ];

    let handCards = [
      new Card(acorns,"K"),
      new Card(acorns,"7"),
      new Card(bells,"10"),
      new Card(hearts,"A")
    ]

    chai.assert.equal(validCard(openCards, handCards, trump).length, 1, "Only King");
    chai.assert.equal(validCard(openCards, handCards, trump, handCards[0]), true, "Play higher Symbol");
    chai.assert.equal(validCard(openCards, handCards, trump, handCards[1]), false, "Play lower Symbol");
    chai.assert.equal(validCard(openCards, handCards, trump, handCards[2]), false, "Play trump by having same color");
    chai.assert.equal(validCard(openCards, handCards, trump, handCards[3]), false, "Play other Color by having same Color");
  })

  it('play Color', function () {
    let trump = bells;

    let openCards = [
      new Card(leaves,"10")
    ];

    let handCards = [
      new Card(acorns,"A"),
      new Card(acorns,"10"),
      new Card(acorns,"7"),
      new Card(leaves,"10"),
      new Card(leaves,"U")
    ]

    chai.assert.equal(validCard(openCards, handCards, trump, handCards[0]), false, "Ass: Need to play leaves");
    chai.assert.equal(validCard(openCards, handCards, trump, handCards[2]), false, "7: Need to play leaves");
    chai.assert.equal(validCard(openCards, handCards, trump, handCards[3]), true);
    chai.assert.equal(validCard(openCards, handCards, trump, handCards[4]), true);
  })
})

describe('valueCards', function () {
  it('each Card', function () {

    let cards = [
      new Card(bells,"A"),
      new Card(bells,"10"),
      new Card(bells,"K"),
      new Card(bells,"O"),
      new Card(bells,"U"),
      new Card(bells,"7"),
    ]

    chai.assert.equal(valueCards(cards), 30);
  })

  it('pair', function () {

    let cards = [
      new Card(bells,"K"),
      new Card(bells,"O")
    ]

    chai.assert.equal(valueCards(cards), 7);
  })

  it('all Cards', function () {

    let cards = shuffledCards();

    chai.assert.equal(valueCards(cards), 240);
  })
})
