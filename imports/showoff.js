import {Card} from './card.js';

export function showoff(cards, trump){

  let result = {
    points: 0,
    sets:[]
  }

  if(hasCards(new Card("l","O"),cards,2)
  && hasCards(new Card("b","U"),cards,2)){
    result.points += 300;
    result.sets.push({
      name: "2 Binokel",
      value: 300,
      cards: [new Card("l","O"), new Card("b","U"),
              new Card("l","O"), new Card("b","U")]
    });
  }else if(hasCards(new Card("l","O"),cards,1)
  && hasCards(new Card("b","U"),cards,1)){
    result.points += 40;
    result.sets.push({
      name: "Binokel",
      value: 40,
      cards: [new Card("l","O"), new Card("b","U")]
    });
  }

  if(hasCards(new Card("a","A"),cards,2)
  && hasCards(new Card("l","A"),cards,2)
  && hasCards(new Card("h","A"),cards,2)
  && hasCards(new Card("b","A"),cards,2)){
    result.points += 1000;
    result.sets.push({
      name: "8 As",
      value: 1000,
      cards: [new Card("a","A"), new Card("l","A"), new Card("h","A"), new Card("b","A"),
              new Card("a","A"), new Card("l","A"), new Card("h","A"), new Card("b","A")]
    });
  }else if(hasCards(new Card("a","A"),cards)
  && hasCards(new Card("l","A"),cards)
  && hasCards(new Card("h","A"),cards)
  && hasCards(new Card("b","A"),cards)){
    result.points += 100;
    result.sets.push({
      name: "4 As",
      value: 100,
      cards: [new Card("a","A"), new Card("l","A"), new Card("h","A"), new Card("b","A")]
    });
  }

  if(hasCards(new Card("a","K"),cards,2)
  && hasCards(new Card("l","K"),cards,2)
  && hasCards(new Card("h","K"),cards,2)
  && hasCards(new Card("b","K"),cards,2)){
    result.points += 1000;
    result.sets.push({
      name: "8 Könige",
      value: 1000,
      cards: [new Card("a","K"), new Card("l","K"), new Card("h","K"), new Card("b","K"),
              new Card("a","K"), new Card("l","K"), new Card("h","K"), new Card("b","K")]
    });
  }else if(hasCards(new Card("a","K"),cards)
  && hasCards(new Card("l","K"),cards)
  && hasCards(new Card("h","K"),cards)
  && hasCards(new Card("b","K"),cards)){
    result.points += 80;
    result.sets.push({
      name: "4 Könige",
      value: 80,
      cards: [new Card("a","K"), new Card("l","K"), new Card("h","K"), new Card("b","K")]
    });
  }

  if(hasCards(new Card("a","O"),cards,2)
  && hasCards(new Card("l","O"),cards,2)
  && hasCards(new Card("h","O"),cards,2)
  && hasCards(new Card("b","O"),cards,2)){
    result.points += 1000;
    result.sets.push({
      name: "8 Ober",
      value: 1000,
      cards: [new Card("a","O"), new Card("l","O"), new Card("h","O"), new Card("b","O"),
              new Card("a","O"), new Card("l","O"), new Card("h","O"), new Card("b","O")]
    });
  }else if(hasCards(new Card("a","O"),cards)
  && hasCards(new Card("l","O"),cards)
  && hasCards(new Card("h","O"),cards)
  && hasCards(new Card("b","O"),cards)){
    result.points += 60;
    result.sets.push({
      name: "4 Ober",
      value: 60,
      cards: [new Card("a","O"), new Card("l","O"), new Card("h","O"), new Card("b","O")]
    });
  }

  if(hasCards(new Card("a","U"),cards,2)
  && hasCards(new Card("l","U"),cards,2)
  && hasCards(new Card("h","U"),cards,2)
  && hasCards(new Card("b","U"),cards,2)){
    result.points += 1000;
    result.sets.push({
      name: "8 Unter",
      value: 1000,
      cards: [new Card("a","U"), new Card("l","U"), new Card("h","U"), new Card("b","U"),
              new Card("a","U"), new Card("l","U"), new Card("h","U"), new Card("b","U")]
    });
  }else if(hasCards(new Card("a","U"),cards)
  && hasCards(new Card("l","U"),cards)
  && hasCards(new Card("h","U"),cards)
  && hasCards(new Card("b","U"),cards)){
    result.points += 40;
    result.sets.push({
      name: "4 Unter",
      value: 40,
      cards: [new Card("a","U"), new Card("l","U"), new Card("h","U"), new Card("b","U")]
    });
  }

  Card.colors.forEach((color) => {
    if(hasCards(new Card(color,"A"),cards,2) && hasCards(new Card(color,"10"),cards,2) && hasCards(new Card(color,"K"),cards,2) && hasCards(new Card(color,"O"),cards,2) && hasCards(new Card(color,"U"),cards,2)){
      if(color == trump){
        result.points += 300;
        result.sets.push({
          name: "2 Trumpf Familien",
          value: 1500,
          cards: [new Card(trump,"A"),new Card(trump,"10"),new Card(trump,"K"),new Card(trump,"O"),new Card(trump,"U"),new Card(trump,"A"),new Card(trump,"10"),new Card(trump,"K"),new Card(trump,"O"),new Card(trump,"U")]
        });
      }else{
        result.points += 200;
        result.sets.push({
          name: "2 "+Card.colorNames[color]+" Familien",
          value: 1500,
          cards: [new Card(color,"A"),new Card(color,"10"),new Card(color,"K"),new Card(color,"O"),new Card(color,"U"),new Card(color,"A"),new Card(color,"10"),new Card(color,"K"),new Card(color,"O"),new Card(color,"U")]
        });
      }
    }else if(hasCards(new Card(color,"A"),cards) && hasCards(new Card(color,"10"),cards) && hasCards(new Card(color,"K"),cards,2) && hasCards(new Card(color,"O"),cards,2) && hasCards(new Card(color,"U"),cards)){
      if(color == trump){
        result.points += 150;
        result.sets.push({
          name: "Trumpf Familie",
          value: 150,
          cards: [new Card(trump,"A"),new Card(trump,"10"),new Card(trump,"K"),new Card(trump,"O"),new Card(trump,"U")]
        });
      }else{
        result.points += 100;
        result.sets.push({
          name: Card.colorNames[color]+" Familie",
          value: 100,
          cards: [new Card(color,"A"),new Card(color,"10"),new Card(color,"K"),new Card(color,"O"),new Card(color,"U")]
        });
      }
      if(color == trump){
        result.points += 40;
        result.sets.push({
          name: "Trumpf Paar",
          value: 40,
          cards: [new Card(trump,"K"),new Card(trump,"O")]
        });
      }else{
        result.points += 20;
        result.sets.push({
          name: Card.colorNames[color]+" Paar",
          value: 20,
          cards: [new Card(color,"K"),new Card(color,"O")]
        });
      }
    }else if(hasCards(new Card(color,"A"),cards) && hasCards(new Card(color,"10"),cards) && hasCards(new Card(color,"K"),cards) && hasCards(new Card(color,"O"),cards) && hasCards(new Card(color,"U"),cards)){
      if(color == trump){
        result.points += 150;
        result.sets.push({
          name: "Trumpf Familie",
          value: 150,
          cards: [new Card(trump,"A"),new Card(trump,"10"),new Card(trump,"K"),new Card(trump,"O"),new Card(trump,"U")]
        });
      }else{
        result.points += 100;
        result.sets.push({
          name: Card.colorNames[color]+" Familie",
          value: 100,
          cards: [new Card(color,"A"),new Card(color,"10"),new Card(color,"K"),new Card(color,"O"),new Card(color,"U")]
        });
      }
    }else if(hasCards(new Card(color,"K"),cards,2) && hasCards(new Card(color,"O"),cards,2)){
      if(color == trump){
        result.points += 80;
        result.sets.push({
          name: "2 Trumpf Paare",
          value: 80,
          cards: [new Card(trump,"K"),new Card(trump,"O")]
        });
      }else{
        result.points += 40;
        result.sets.push({
          name: "2 "+Card.colorNames[color]+" Paare",
          value: 40,
          cards: [new Card(color,"K"),new Card(color,"O")]
        });
      }
    }else if(hasCards(new Card(color,"K"),cards) && hasCards(new Card(color,"O"),cards)){
      if(color == trump){
        result.points += 40;
        result.sets.push({
          name: "Trumpf Paar",
          value: 40,
          cards: [new Card(trump,"K"),new Card(trump,"O")]
        });
      }else{
        result.points += 20;
        result.sets.push({
          name: Card.colorNames[color]+" Paar",
          value: 20,
          cards: [new Card(color,"K"),new Card(color,"O")]
        });
      }
    }
  });

  if(hasCards(new Card(trump,"7"),cards,2)){
    result.points += 20;
    result.sets.push({
      name: "2 Diss",
      value: 20,
      cards: [new Card(trump,"7"),
              new Card(trump,"7")]
    });
  }else if(hasCards(new Card(trump,"7"),cards)){
    result.points += 10;
    result.sets.push({
      name: "Diss",
      value: 10,
      cards: [new Card(trump,"7")]
    });
  }


  return result;
}

// check if given card is n times in cards
function hasCards(card,cards,n = 1){
  let found = 0;
  cards.forEach((item) => {
    if(card.isEqual(item)){
      found++;
    }
  });
  return found >= n;
}
