import chai from 'chai';
import {Card} from '../imports/card.js'

describe('construct', function () {
  it('toString', function () {
    chai.assert.equal((new Card("a","A")).toString(), "a-A");
    chai.assert.equal((new Card("l","10")).toString(), "l-10");
    chai.assert.equal((new Card("h","U")).toString(), "h-U");
  })

  it('to base64', function () {
    let card = new Card("a","A");
    chai.assert.equal(card.toBase64(), "YS1B");

    card = new Card("l","10");
    chai.assert.equal(card.toBase64(), "bC0x");
  })

  it('from base64', function () {
    let card = new Card("YS1B");
    chai.assert.equal(card.toString(), "a-A");

    card = new Card("bC0x");
    chai.assert.equal(card.toString(), "l-10");
  })
})


describe('interaction', function () {
  it('isEqual', function () {
    let card1 = new Card("h","K");
    let card2 = new Card("h","K");
    let card3 = new Card("l","K");
    let card4 = new Card("h","10");

    chai.assert.equal(card1.isEqual(card2), true, "Card1 equals Card2");
    chai.assert.equal(card1.isEqual(card3), false, "Card1 equals not Card3");
    chai.assert.equal(card1.isEqual(card4), false, "Card1 equals not Card4");
  })

  it('isHigherThan', function () {
    let card1 = new Card("h","K");
    let card2 = new Card("h","K");
    let card3 = new Card("l","U");
    let card4 = new Card("l","O");

    chai.assert.equal(card1.isHigherThan(card2,"l"), false, "Card1 equals Card2");
    chai.assert.equal(card3.isHigherThan(card1,"l"), true,  "Card3 is trump");
    chai.assert.equal(card4.isHigherThan(card3,"l"), true, "Card4 have higher Symbol");
  })
})
