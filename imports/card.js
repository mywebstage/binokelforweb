export class Card{

  static colors = ["b","h","l","a"] //  bells, hearts, leaves, acorns
  static symbols = ["7","U","O","K","10","A"]
  static colorNames = {
    "b": "Schellen",
    "h": "Herz",
    "l": "Schippe",
    "a": "Kreuz",
  }
  static values = {
    "7": 0,
    "U": 2,
    "O": 3,
    "K": 4,
    "10": 10,
    "A": 11
  }

  constructor(color_or_base64 = false, symbol = false){
    if(symbol){
      this.setColor(color_or_base64);
      this.setSymbol(symbol);
    }else if(color_or_base64) {
      this.fromBase64(color_or_base64);
    }
  }

  value(){
    return Card.values[this.symbol];
  }

  isEqual(card){
    return this.color == card.color && this.symbol == card.symbol;
  }

  isValid(){
    return Card.colors.includes(this.color) && Card.symbols.includes(this.symbol);
  }

  isHigherThan(card,trump = false){
    if(this.color == card.color){
      if(Card.symbols.indexOf(this.symbol) > Card.symbols.indexOf(card.symbol)){
        return true;
      }
    }else if(trump && this.color == trump){ // else of same color -> first card was not trump
      return true;
    }

    return false;
  }

  toString(){
    return this.color+"-"+this.symbol;
  }

  toBase64(){
    let symbol = this.symbol == "10" ? "1" : this.symbol;
    if(typeof btoa == 'function'){
      return btoa(this.color+"-"+symbol); // for Browser
    }else{
      return Buffer.from(this.color+"-"+symbol).toString('base64'); // for NodeJS
    }
  }

  fromBase64(base64){
    if(typeof atob == 'function'){
      var data = atob(base64).split("-"); // for Browser
    }else{
      var data = Buffer.from(base64, 'base64').toString('ascii').split("-"); // for NodeJS
    }

    let symbol = data[1] == "1" ? "10" : data[1];

    this.setColor(data[0]);
    this.setSymbol(symbol);
  }

  setColor(color) {
    if(Meteor.isClient && !Card.colors.includes(color)){
      console.error("Color <"+color+"> is not valid");
    }
    this.color = color;
  }

  setSymbol(symbol) {
    if(Meteor.isClient && !Card.symbols.includes(symbol)){
      console.error("Symbol <"+symbol+"> is not valid");
    }
    this.symbol = symbol;
  }



}
