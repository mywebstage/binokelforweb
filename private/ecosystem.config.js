module.exports = {
  "apps":[{
    "name":"binokel",
    "script":"binokel/bundle/main.js",
    "watch":true,
    "instances":1,
    "env":{
      "NODE_ENV":"production",
      "MONGO_URL":"mongodb+srv://USER:PASS@cluster.mongodb.net/Binokel",
      "ROOT_URL":"https://binokel.familie-hehn.de" ,
      "HTTP_FORWARDED_COUNT":2,
      "PORT":"8080"
    }
  }]
};
