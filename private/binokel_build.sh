rm -rf binokelforweb

# Clone Repository
git clone -b master --single-branch https://bitbucket.org/mywebstage/binokelforweb.git

#Build meteor package
cd binokelforweb
meteor npm install --allow-superuser
meteor build ../ --architecture os.linux.x86_64 --server-only --allow-superuser

# root folder
cd ../

rm -rf binokel
mkdir binokel

mv binokelforweb.tar.gz binokel/binokelforweb.tar.gz
cd binokel
tar xvfz binokelforweb.tar.gz
rm -rf binokelforweb.tar.gz

# installing bundle
cd bundle/programs/server
npm install
npm install bcrypt

# root folder
cd ~
pm2 restart ecosystem.config.js --update-env
pm2 save
