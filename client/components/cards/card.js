import './card.html';
import { Card } from '/imports/card.js';

Template.card.onRendered(()=>{

});

Template.card.events({

});

Template.card.helpers({
  'card'(){
    if(this.base64){
      return new Card(this.base64);
    }else if(this.color && this.symbol){
      return new Card(this.color, this.symbol);
    }
  }
});
