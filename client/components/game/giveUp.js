import './giveUp.html';
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

Template.giveUp.onRendered(()=>{
  M.Modal.init(document.querySelectorAll('.modal'));
});

Template.giveUp.events({
  'click #giveUpSubmit'(){
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call("games.giveUp",gameId);
  },
});

Template.giveUp.helpers({
  'addPoints'(){
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    if(game){
      return game.nrPlayers * 10;
    }
  }
});
