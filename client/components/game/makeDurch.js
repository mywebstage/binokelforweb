import './makeDurch.html';
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

Template.makeDurch.onRendered(()=>{
  M.Modal.init(document.querySelectorAll('.modal'));
});

Template.makeDurch.events({
  'click #durchSubmit'(){
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call("games.durch",gameId);
  },
});

Template.makeDurch.helpers({

});
