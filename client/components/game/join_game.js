import './join_game.html';
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

Template.join_game.onRendered(()=>{
});

Template.join_game.events({
  'click .changeTeam'(e){
    let gameId = FlowRouter.getParam('gameId');
    let team = Number($(e.currentTarget).attr("team"));
    Meteor.call("games.changeTeam",gameId,team);
  },
  'click #startGame'(){
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call("games.start",gameId,(err,data)=>{
      if(data && data.open_slots){
        M.toast({html:"Es fehlen noch "+data.open_slots+" Mitspieler!"})
      }
    });

  },
  'click #leaveGame'(){
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call("games.leave",gameId,()=>{
      FlowRouter.go("/");
    });
  }
});

Template.join_game.helpers({
  nrTeams(){
    let game = Games.findOne(FlowRouter.getParam('gameId'));
    if(game)
    return game.nrTeams;
  },
  getTeam(teamNr){
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    if(game){
      let perTeam = game.nrPlayers / game.nrTeams;
      let players = [];
      Players.find({teamNr,gameId}).forEach((player)=>{
        players.push(player);
      });
      let open_slots = perTeam - players.length;

      for(let i = 0; i < open_slots; i++){
        players.push({open:true});
      }
      return players;
    }    
  }
});
