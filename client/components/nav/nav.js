import './nav.html';

Template.nav.onRendered(function(){
  M.Sidenav.init(document.querySelectorAll('.sidenav'));
  M.Dropdown.init(document.querySelectorAll('.dropdown-trigger'), {
    coverTrigger: false,
    alignment: 'right',
    constrainWidth: false
  });
});

Template.nav.events({
  'click #logout'(){
    Meteor.logout();
  },
  'click .sidenav a'(e){
    e.preventDefault();
    document.querySelectorAll('.sidenav').forEach((ele) => {
      M.Sidenav.getInstance(ele).close();
    });
        
    FlowRouter.go($(e.currentTarget).attr("href"));
  }
});

Template.nav.helpers({
  pages() {
    let pages = [
      {
        title: "Spiele",
        route: "/",
        active: FlowRouter.getRouteName() == "games" ? "active" : "",
      },
      {
        title: "Freunde",
        route: "/friends",
        active: FlowRouter.getRouteName() == "friends" ? "active" : "",
      },
      {
        title: "Regeln",
        route: "/rules",
        active: FlowRouter.getRouteName() == "rules" ? "active" : "",
      }
    ];

    return pages;
  }
});
