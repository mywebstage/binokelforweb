import './trump.html';
import '../game/roundInfo.js'
import '../cards/card.html';
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

Template.trump.onRendered(()=>{

});

Template.trump.events({
  'click .trumpCard'(e){
    let trump = $(e.currentTarget).attr("color");
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call("games.setTrump",gameId,trump);
  }
});

Template.trump.helpers({
  'trumpCards'(){
    return [
      {color: "a", symbol: "A"},
      {color: "l", symbol: "A"},
      {color: "h", symbol: "A"},
      {color: "b", symbol: "A"}
    ]
  },
  'handCards'(){
    let gameId = FlowRouter.getParam('gameId');
    let userId = Meteor.userId();
    let player = Players.findOne({gameId,userId});
    if(player){
      return player.cards;
    }
  }
});
