import './showoff.html';
import '../game/roundInfo.js';
import '../game/makeDurch.js';
import '../cards/card.html';
import { showoff } from '/imports/showoff.js';
import { Card } from '/imports/card.js';
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

let showoffResult = new ReactiveVar({points:0});

Template.showoff.onRendered(()=>{
  showoffResult.set({points:0});
});

Template.showoff.events({
  'click .handCard'(e){
    if($(e.currentTarget).hasClass("selected")){
      $(e.currentTarget).removeClass("selected");
    }else{
      $(e.currentTarget).addClass("selected");
    }
    update_showoff();
  },
  'click #showoffAll'(){
    $(".handCard").addClass("selected");
    update_showoff();
    confirm_showoff();
    $(".handCard").removeClass("selected");
  },
  'click #showoff'(){
    let result = showoffResult.get();
    confirm_showoff();
    $(".handCard").removeClass("selected");
  },
  'click #showoffReady'(){
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call("games.showoffReady",gameId);
  }
});

Template.showoff.helpers({
  'handCards'(){
    let gameId = FlowRouter.getParam('gameId');
    let userId = Meteor.userId();
    let player = Players.findOne({gameId,userId});
    if(player){
      return player.cards;
    }
  },
  'showoffResult'(){
    return showoffResult.get();
  },
  'showoffSummary'(){
    let gameId = FlowRouter.getParam('gameId');
    let userId = Meteor.userId();
    let myPlayer = Players.findOne({gameId,userId});
    if(myPlayer && myPlayer.showoff){
      let allPlayers = [];
      Players.find({gameId},{fields:{
        userId: 1,
        showoff: 1
      }}).forEach((player)=>{
        if(player.userId == "bot"){
          player.name = "Bot";
        }else{
          let user = Meteor.users.findOne(player.userId);
          player.name = user && user.username ? user.username : "User";
        }
        allPlayers.push(player);
      });
      return allPlayers;
    }else{
      return false;
    }
  },
  'showoffReady'(){
    let ready = true;
    let gameId = FlowRouter.getParam('gameId');
    Players.find({gameId},{fields:{
      showoff: 1
    }}).forEach((player)=>{
      ready = ready && player.showoff;
    });
    return ready;
  },
  'simpleMode'(){
    let game = Games.findOne(FlowRouter.getParam('gameId'));
    if(game){
      return game.simpleMode;
    }
  },
  'haveDapp'(){
    let game = Games.findOne(FlowRouter.getParam('gameId'));
    if(game){
      return game.dapp;
    }
  },
  'waiting'(){
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    return game && game.waiting && game.waiting.includes(Meteor.userId());
  }
});

function update_showoff(){
  let game = Games.findOne(FlowRouter.getParam('gameId'));
  if(game){
    let round = game.rounds.pop();
    let selectedCards = [];
    $(".handCard.selected").toArray().forEach((cardElement) => {
      selectedCards.push(new Card(cardElement.attributes.base64.value));
    });
    showoffResult.set(showoff(selectedCards,round.trump));
  }
}

function confirm_showoff(){
  let gameId = FlowRouter.getParam('gameId');
  let cards = [];
  $(".handCard.selected").toArray().forEach((item) => {
    let card = new Card(item.attributes.base64.value);
    if(card.isValid()){
      cards.push(card.toBase64());
    }
  });
  Meteor.call("games.setShowoff",gameId,cards);
}
