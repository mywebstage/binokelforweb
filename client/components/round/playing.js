import './playing.html';
import '../cards/card.html';
import '../game/roundInfo.js'
import { validCard64 } from '/imports/game.js';
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

let playableCard = new ReactiveVar(false);

Template.playing.onRendered(()=>{
  playableCard.set(false);
});

Template.playing.events({
  'click .handCard'(e){
    if($(e.currentTarget).hasClass("selected") || $(e.currentTarget).hasClass("unvalid")){
      $(e.currentTarget).removeClass("selected");
    }else{
      $('.handCard.selected').removeClass("selected");
      $(e.currentTarget).addClass("selected");
    }

    playableCard.set($(".handCard.selected").toArray().length == 1);

  },
  'click #playCard'(e){
    let gameId = FlowRouter.getParam('gameId');
    let userId = Meteor.userId();
    let player = Players.findOne({gameId,userId});
    let round = this;
    if(player){
      let selectedCards = [];
      let card = $(".handCard.selected").toArray()[0];
      let selectedCard = card.attributes.base64.value;

      if(validCard64(round.stack, player.cards, round.trump, selectedCard)){
        Meteor.call("games.playCard",gameId,selectedCard,()=>{
          $('.handCard.selected').removeClass("selected");
          playableCard.set(false);
        });
      }else{
        M.toast({html:"Diese Karte ist nicht erlaubt"});
      }

    }
  },
  'click #nextStitch'(e){
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call("games.nextStitch",gameId);
  },

});

Template.playing.helpers({
  'handCards'(){
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    let userId = Meteor.userId();
    let player = Players.findOne({gameId,userId});
    let round = this;
    if(round.currentPlayer && game && player){
      let validCards = validCard64(round.stack, player.cards, round.trump);

      let cards = [];
      player.cards.forEach((base64)=>{
        let css = "handCard";
        if(round.currentPlayer.userId != userId || round.stack.filter((stackCard)=>stackCard.playerId == player._id).length > 0){
          css += " disabled";
        }else if(game.simpleMode && !validCards.includes(base64)){
          css += " disabled unvalid";
        }
        cards.push({base64,class:css});
      });

      return cards;
    }
  },
  'winner'(){
    let round = this;
    if(round.winnerCard){
      let player = Players.findOne(round.winnerCard.playerId);
      if(player){
        return player.userId;
      }
    }
  },
  'playableCard'(){
    return playableCard.get();
  },
  'remainingPlayers'(){
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    let round = this;
    let userIds = [];
    if(game){
      for(let p = 0; p < game.nrPlayers - round.stack.length; p++){
        let position = (p + round.currentPlayer.position - 1) % game.nrPlayers + 1;
        let player = Players.findOne({gameId,position});
        if(player && player.userId){
          userIds.push(player.userId);
        }
      }
    }
    return userIds;
  }
});
