import './bet.html';
import '../game/roundInfo.js'
import '../cards/card.js';
import { next_step_of } from '/imports/functions.js';
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

Template.bet.onRendered(()=>{

});

Template.bet.events({
  'click .bid'(e){
    let gameId = FlowRouter.getParam('gameId');
    let bid = Number($(e.currentTarget).attr("bid"));
    Meteor.call('games.bid',gameId,bid);
    M.toast({html: '<span>Gebot '+bid+' wurde abgegeben...</span>'});
  },
  'click #i_am_out'(){
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call('games.bid_out',gameId);
  }
});

Template.bet.helpers({
  'bedding'(){
    let round = this;
    let userId = Meteor.userId();
    if(round && round.outbid){
      return !round.outbid.includes(userId);
    }
  },
  'next_step_of'(bid, stepsize){
    return next_step_of(bid, stepsize);
  },
  'handCards'(){
    let gameId = FlowRouter.getParam('gameId');
    let userId = Meteor.userId();
    let player = Players.findOne({gameId,userId});
    if(player){
      return player.cards;
    }
  }
});
