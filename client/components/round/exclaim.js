import './exclaim.html';
import '../game/roundInfo.js'
import '../cards/card.js';
import { Card } from '/imports/card.js';
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

let exclaimColor = new ReactiveVar(false);
let exclaimSymbol = new ReactiveVar("?");
let exclaimReturnCard = new ReactiveVar(false);

Template.exclaim.onRendered(()=>{
  exclaimColor.set(false);
  exclaimSymbol.set(false);
  exclaimReturnCard.set(false);
});

Template.exclaim.events({
  'click .exclaimCard'(e){
    if(exclaimColor.get()){
      let symbol = $(e.currentTarget).attr("symbol");
      exclaimSymbol.set(symbol);
    }else{
      let color = $(e.currentTarget).attr("color");
      exclaimColor.set(color);
    }
  },
  'click .handCard'(e){
    if($(e.currentTarget).hasClass("selected")){
      $(e.currentTarget).removeClass("selected");
      exclaimReturnCard.set(false);
    }else{
      $('.handCard.selected').removeClass("selected");
      $(e.currentTarget).addClass("selected");
      exclaimReturnCard.set($(e.currentTarget).attr("base64"));
    }
  },
  'click #exclaim'(){
    let card = new Card(exclaimColor.get(),exclaimSymbol.get());
    let returnCard = exclaimReturnCard.get();
    if(returnCard && card.isValid()){
      let gameId = FlowRouter.getParam('gameId');
      Meteor.call('games.exclaim',gameId,card.toBase64(),returnCard,(err,data)=>{
        if(data && data.error){
          M.toast({html:data.error})
        }
      })
    }
  },
  'click #acceptExclaim'(){
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call('games.acceptExclaim',gameId);
  },
  'click #changeColor'(){
    exclaimColor.set(false);
    exclaimSymbol.set(false);
  }
});

Template.exclaim.helpers({
  'exclaimCards'(){
    if(!exclaimColor.get()){
      let cards = [];
      Card.colors.forEach((color) => {
        cards.push({color,symbol:"A",css:"exclaimCard"})
      });
      return cards;
    }else{
      let cards = [];
      Card.symbols.forEach((symbol) => {
        let css = "exclaimCard";
        if(symbol == exclaimSymbol.get()){
          css += " selected";
        }
        cards.unshift({color:exclaimColor.get(),symbol,css})
      });
      return cards;
    }
  },
  'exclaimReady'(){
    return exclaimColor.get() && exclaimSymbol.get() && exclaimReturnCard.get();
  },
  'exclaimColor'(){
    return exclaimColor.get();
  },
  'exclaimSymbol'(){
    return exclaimSymbol.get();
  },
  'handCards'(){
    let gameId = FlowRouter.getParam('gameId');
    let userId = Meteor.userId();
    let player = Players.findOne({gameId,userId});
    if(player){
      return player.cards;
    }
  }
});
