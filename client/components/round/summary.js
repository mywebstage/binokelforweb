import './summary.html';
import '../cards/card.html';
import { valueCards64, pointsOfPlayer, pointsOfTeam, stichesOfTeam } from '/imports/game.js';
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';


Template.summary.onRendered(()=>{

});

Template.summary.events({
  'click #nextRound'(){
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call('games.nextRound',gameId);
  },
  'click #endGame'(){
    let gameId = FlowRouter.getParam('gameId');
    Meteor.call('games.end',gameId);
    FlowRouter.go("games");
  }
});

Template.summary.helpers({
  'playerResults'(){
    let gameId = FlowRouter.getParam('gameId');

    let players = {
      userId: [],
      showoff: [],
      stiches: [],
      lastStitch: [],
      sum: [],
      result: []
    }

    let round = this;
    let leader = Players.findOne({gameId,userId:round.leader});
    if(leader){


      Players.find({gameId},{sort:{teamNr:1}}).forEach((player) => {
        players.userId.push(player.userId);
        players.showoff.push({points:player.showoff.points});
        players.lastStitch.push({points:player.lastStitch ? 10 : 0});
        players.stiches.push({points:valueCards64(player.stitches)});

        let points = 0;
        if(stichesOfTeam(Players.find({gameId,teamNr:player.teamNr}))){
          points = pointsOfPlayer(player);
        }

        players.sum.push({points});

        if(player.teamNr == leader.teamNr){
          if(round.won){ // will be calculated by server
            players.result.push({points:round.durch ? 1000 : points});
          }else if (player._id == leader._id) {
            players.result.push({points:round.bid * (round.giveUp ? -1 : -2)});
          }else{
            players.result.push({points:0});
          }
        }else{
          players.result.push({points});
        }
      });
    }
    return players;
  },
  'teams'(){
    let gameId = FlowRouter.getParam('gameId');
    return teamsOfGame(gameId);
  },
  'winner'(){
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    let points = game.goal;
    let winner = false
    teamsOfGame(gameId).forEach((team) => {
      if(team.points >= points){
        winner = team.name;
        points = team.points;
      }
    });
    return winner;
  },
  'waiting'(){
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    return game && game.waiting && game.waiting.includes(Meteor.userId());
  },
  'goal'(){
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    if(game){
      return game.goal;
    }
  }
});


function teamsOfGame(gameId){
  let game = Games.findOne(gameId);

  let teams = [];

  if(game){
    let round = game.rounds.pop();
    let leader = Players.findOne({gameId,userId:round.leader});

    if(leader){
      for(let teamNr = 1; teamNr <= game.nrTeams; teamNr++){
        let teamName = [];
        let points = 0;

        Players.find({gameId,teamNr},{sort:{position:1}}).forEach((player)=>{
          points += player.points;
          if(player.userId == "bot"){
            teamName.push("Bot");
          }else{
            let user = Meteor.users.findOne({_id:player.userId});
            if(user){
              teamName.push(user.username);
            }
          }
        })

        let thisPoints = pointsOfTeam(Players.find({gameId,teamNr}));
        if(teamNr == leader.teamNr){
          if(round.won && round.durch){
            thisPoints = 1000;
          }else if(!round.won){
            thisPoints = round.bid * (round.giveUp ? -1 : -2);
          }
        }

        teams.push({
          name: teamName.join(" und "),
          lastPoints: points - thisPoints,
          thisPoints,
          points
        })
      }
    }
  }
  return teams.sort((teamA,teamB)=>teamB.points - teamA.points);
}
