import './dapp.html';
import '../game/giveUp.js';
import '../game/makeDurch.js';
import '../game/roundInfo.js'
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';

let selectedDapp = ReactiveVar(0);

Template.dapp.onRendered(()=>{
  selectedDapp.set(0);
});

Template.dapp.events({
  'click .handCard'(e){
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    let round = game.rounds.pop();
    if(round.leader == Meteor.userId()){
      if($(e.currentTarget).hasClass("selected")){
        $(e.currentTarget).removeClass("selected");
      }else{
        $(e.currentTarget).addClass("selected");
      }
      selectedDapp.set($(".handCard.selected").length);
    }
  },
  'click #confirmDapp'(){
    let cards = [];
    $(".handCard.selected").toArray().forEach((card) => {
      let base64 = $(card).attr("base64");
      cards.push(base64);
    });

    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    let round = game.rounds.pop();
    if(cards.length == round.dapp.length){
      Meteor.call("games.setDapp",gameId,cards);
    }


  }
});

Template.dapp.helpers({
  'handCards'(){
    let gameId = FlowRouter.getParam('gameId');
    let userId = Meteor.userId();
    let player = Players.findOne({gameId,userId});
    let game = Games.findOne(gameId);

    if(player && game){
      let round = game.rounds.pop();
      let cards = [];

      player.cards.forEach((base64)=>{
        let css = "handCard";

        // Add class "dappCard" to make the blue border for leader
        if(round.leader == userId && round.dapp.includes(base64)){
          css += " dappCard";
          round.dapp.splice(round.dapp.indexOf(base64), 1);
        }
        cards.push({base64,class:css});
      });
      return cards;
    }
  },
  'selectedDapp'(){
    return selectedDapp.get();
  }
});
