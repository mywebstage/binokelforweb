import './register.html';

Template.register.onRendered(()=>{

});

Template.register.events({
  'click #register-button'(){
    let username = $("#username").val();
    let password = $("#password").val();

    Accounts.createUser({
      username,
      password
    },(err)=>{
      if(err){
        M.toast({html:err.reason})
      }else{
        FlowRouter.go("/");
      }
    });
  },
});

Template.register.helpers({

});
