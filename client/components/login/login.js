import './login.html';

Template.login.onRendered(()=>{

});

Template.login.events({
  'click #login-button'(){
    submitLogin();
  },
  'keyup'(event){
    if(event.key == "Enter"){
      submitLogin();
    }
  }
});

Template.login.helpers({

});

function submitLogin(){
  let username = $("#username").val();
  let password = $("#password").val();

  Meteor.loginWithPassword(username,password,(error)=>{
    if(error){
      $("input").addClass("invalid");
      M.toast({html:"Benutzername oder Passwort falsch!"});
    }
  });
}
