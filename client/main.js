// Client entry point, imports all client code
import '/imports/functions.js';
import './routes.js';
import './helpers.js';

Meteor.subscribe("self");
Meteor.subscribe("friends");
Meteor.subscribe("games");
Meteor.subscribe("my_players");


(function () {
    var func = EventTarget.prototype.addEventListener;

    EventTarget.prototype.addEventListener = function (type, fn, capture) {
        this.func = func;
        capture = capture || {};
        capture.passive = false;
        this.func(type, fn, capture);
    };
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
      console.log('ServiceWorker registration successful!');
    }).catch(function(err) {
      console.log('ServiceWorker registration failed: ', err);
    });
  }

}());
