import './internal_body.html';
import '../../components/nav/nav.js';
import '../../components/login/login.js';
import '../../components/login/register.js';

Template.internal_body.onRendered(function(){

});

Template.internal_body.events({

});

Template.internal_body.helpers({
  subscriptionReady(){
    return Meteor.users.find().count() > 0;
  }
});
