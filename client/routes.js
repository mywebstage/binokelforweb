import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Import layout
import './layouts/body/external_body.js';
import './layouts/body/internal_body.js';

//import Pages
import './pages/users/profile.js';
import './pages/users/friends.js';
import './pages/games/game.js';
import './pages/games/games.js';
import './pages/rules/rules.js';
import './pages/notFound/notFound.js';

FlowRouter.route('/', {
  name: 'games',
  action() {
    BlazeLayout.render('internal_body', { page: 'games' });
  },
});

FlowRouter.route('/profile', {
  name: 'profile',
  action() {
    BlazeLayout.render('internal_body', { page: 'profile' });
  },
});

FlowRouter.route('/friends', {
  name: 'friends',
  action() {
    BlazeLayout.render('internal_body', { page: 'friends' });
  },
});

FlowRouter.route('/game/:gameId', {
  name: 'game',
  action() {
    BlazeLayout.render('internal_body', { page: 'game' });
  },
});

FlowRouter.route('/rules', {
  name: 'rules',
  action() {
    BlazeLayout.render('external_body', { page: 'rules' });
  },
});

FlowRouter.route('/register', {
  name: 'register',
  action() {
    BlazeLayout.render('external_body', { page: 'register' });
  },
});


FlowRouter.notFound = {
  action() {
    BlazeLayout.render('external_body', { page: 'notFound' });
  },
};
