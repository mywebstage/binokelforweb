import {dateFormat} from '/imports/functions.js';
import {Players} from '/imports/collections/players.js'

Template.registerHelper('equal', (a,b) => {
  return a == b;
});
Template.registerHelper('selected_if_equal', (a,b) => {
  return a == b ? "selected" : "";
});
Template.registerHelper('disabled_if_equal', (a,b) => {
  return a == b ? "disabled" : "";
});
Template.registerHelper('range', (start, end) => {
  let range = [];
  for(let i = 0; i <= end-start; i++){
    let  n = start + i;
    range.push({i, n});
  }

  return range;
});

Template.registerHelper('userId', () => {
  return Meteor.userId();
});

Template.registerHelper('username', (userId) => {
  if(userId == "bot"){
    return "Bot";
  }
  let username = "User";
  let user = Meteor.users.findOne(userId);
  if(user){
    username = user.username
  }
  return username;
});
Template.registerHelper('playername', (playerId) => {
  let playername = "Player";
  let player = Players.findOne(playerId);
  if(player){
    if(player.userId == "bot"){
      return "Bot";
    }
    let user = Meteor.users.findOne(player.userId);
    if(user){
      playername = user.username;
    }
  }

  return playername;
});

Template.registerHelper('the_date', (date) => {
  return dateFormat(date,dateFormat.masks.longDate);
});
Template.registerHelper('the_time', (date) => {
  return dateFormat(date,dateFormat.masks.time);
});
Template.registerHelper('the_dateTime', (date) => {
  return dateFormat(date,dateFormat.masks.dateTime);
});
Template.registerHelper('count', (cursor) => {
  return cursor.count();
});
