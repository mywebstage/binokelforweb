import './profile.html';

Template.profile.onRendered(()=>{
  M.Modal.init(document.getElementById('deleteModal'));
});

Template.profile.events({
  'change #username'(){
    Meteor.call('users.setUsername',$('#username').val(),(res)=>{
      if(res == "username_exists"){
        M.toast({html:"Benutzername existiert bereits"});
      }
      if(res == "username_empty"){
        M.toast({html:"Benutzername darf nicht leer sein"});
      }
    });
  },
  'click #changePassword'(){
    let currentPassword = $('#currentPassword').val();
    let newPassword = $('#newPassword').val();
    let newPassword2 = $('#newPassword2').val();
    if (newPassword != newPassword2){
      M.toast({html:"Neue Passwörter stimmen nicht überein"});
    }else{
      Accounts.changePassword(currentPassword, newPassword, (err)=>{
        if(err){
          M.toast({html:err.reason});
        }else{
          M.toast({html:"Passwort geändert"});
          $('#currentPassword').val("");
          $('#newPassword').val("");
          $('#newPassword2').val("");
        }
      });
    }
  },
  'click #deleteSubmit'(){
    M.Modal.getInstance(document.getElementById('deleteModal')).close();
    Meteor.call('users.delete',()=>{
      FlowRouter.go("/");
      Meteor.logout();
    });
  }
});

Template.profile.helpers({
  get_user(){
    return Meteor.user();
  }
});
