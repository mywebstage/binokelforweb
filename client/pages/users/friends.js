import './friends.html';

let addFriendAutocomplete;

Template.friends.onRendered(()=>{
  M.Modal.init(document.getElementById('addFriendModal'));
  addFriendAutocomplete = M.Autocomplete.init(document.getElementById('addFriendAutocomplete'));
  Tracker.autorun(()=>{
    let user = Meteor.user();
    if(user && user.friends){
      Meteor.subscribe("friendRequests",user.friends)
    }

  })
});

Template.friends.events({
  'keyup #addFriendAutocomplete'(){
    let username = $("#addFriendAutocomplete").val();
    if(username.length == 2){
      Meteor.call("users.getUsernames",username,(err,users)=>{
        let data = {};
        users.forEach((user)=>{
          data[user.username] = null;
        })
        delete data[Meteor.user().username];
        addFriendAutocomplete.updateData(data);
      });
    }
  },
  'click #addFriend'(){
    let username = $("#addFriendAutocomplete").val();
    Meteor.call("users.addFriend",username,(err,data)=>{
      if(data && data.username){
        M.toast({html: data.username + " als Freund hinzugefügt!"})
      }else{
        M.toast({html: username + " nicht gefunden!"})
      }
      $("#addFriendAutocomplete").val("");
    });
  },
  'click .addDiffFriend'(e){
    let username = $(e.currentTarget).attr("username");
    Meteor.call("users.addFriend",username,(err,data)=>{
      if(data && data.username){
        M.toast({html: data.username + " als Freund hinzugefügt!"})
      }else{
        M.toast({html: username + " nicht gefunden!"})
      }
    });
  },
});

Template.friends.helpers({
  get_friends(){
    return Meteor.users.find({friends: Meteor.userId()},{sort:{username:1}});
  },
  get_friendsDiff(){
    let me = Meteor.user();
    let diff = [];
    Meteor.users.find({_id:{ $ne: me._id }},{sort:{username:1}}).forEach((user) => {
      if(!user.friends || !user.friends.includes(me._id)){
        diff.push(user);
      }
    });
    return diff;
  }
});
