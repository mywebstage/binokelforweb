import './game.html';
import '../../components/game/join_game.js';
import '../../components/round/bet.js';
import '../../components/round/trump.js';
import '../../components/round/dapp.js';
import '../../components/round/exclaim.js';
import '../../components/round/showoff.js';
import '../../components/round/playing.js';
import '../../components/round/summary.js';
import '../../components/cards/card.html';
import { Games } from '/imports/collections/games.js';
import { Players } from '/imports/collections/players.js';
import { cardsFromBase64 } from '/imports/game.js';

Template.game.onRendered(()=>{

  Tracker.autorun(()=>{
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    if(game){
      Meteor.subscribe('players', game.playerIds);
      Meteor.subscribe('users', game.userIds);
    }else if(gameId){ // Nessesary to switch routes
      FlowRouter.go("games");
    }
  })
});

Template.game.events({

});

Template.game.helpers({
  // get current round Number.
  // 0 stands for not running, 1 stands for first running round...
  'thisRound'(){
    let gameId = FlowRouter.getParam('gameId');
    let game = Games.findOne(gameId);
    if(game){
      if(game.rounds && game.rounds.length > 0){
        let round = game.rounds.pop();
        round.currentPlayer = Players.findOne({gameId,position:round.currentPlayer});
        return round;
      }
      return {join:true};
    }
  },

});
