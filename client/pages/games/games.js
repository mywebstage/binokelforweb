import './games.html';
import { Games } from '/imports/collections/games.js';
import { ReactiveVar } from 'meteor/reactive-var';

import {playerConstellations, playerTeamsConstellations} from '/imports/game.js';

let nrPlayers = ReactiveVar(3);
let nrTeams = ReactiveVar(3);

Template.games.onRendered(()=>{
  M.Modal.init(document.getElementById('addGameModal'));
  Tracker.autorun(()=>{
    let user = Meteor.user();
    if(user && user.friends && user.friends.length){
      Meteor.subscribe("gamesOfFriends",user.friends);
    }

  })
});

Template.games.events({
  'input #nrPlayers'(){
    let nr = Number($("#nrPlayers").val());
    if(nr >= 0 && nr < playerConstellations.length){
      numPlayer = playerConstellations[nr];
      nrPlayers.set(numPlayer);
      nrTeams.set(playerTeamsConstellations[numPlayer][0]);
    }
    if(numPlayer == 5 || numPlayer == 8){
      $("#dapp").prop('disabled', true);
      $('#dapp').prop('checked', numPlayer == 5);
    }else{
      $("#dapp").prop('disabled', false);
    }
  },
  'input #nrTeams'(){
    let nr = Number($("#nrTeams").val());
    if(nr >= 0 && nr < playerTeamsConstellations[nrPlayers.get()].length){
      nrTeams.set(playerTeamsConstellations[nrPlayers.get()][nr]);
    }
  },
  'click #createGame'(){
    let name = $("#name").val();
    let private = $("#private")[0].checked;
    let simpleMode = $("#simpleMode")[0].checked;
    let exclaim = $("#exclaim")[0].checked;
    let dapp = $("#dapp")[0].checked;
    let bot = $("#bot")[0].checked;
    let goal = Number($("#goal").val());

    Meteor.call('games.create', name, private, simpleMode, exclaim, dapp, nrTeams.get(),nrPlayers.get(),goal,bot,(err,data)=>{
      if(data.gameId){
        FlowRouter.go("/game/"+data.gameId);
      }
    })
  }
});

Template.games.helpers({
  get_mygames(){
    let userId = Meteor.userId();
    return Games.find({userIds: userId});
  },
  get_gamesOfFriends(){
    let user = Meteor.user();
    if(user.friends){
      return Games.find({
        $and: [
          {userIds: {$elemMatch: { $in: user.friends } }},
          {rounds: {$size: 0}}
        ]
      });
    }
  },
  get_publicgames(){
    let user = Meteor.user();
    return Games.find({
      $and: [
        {userIds: {$elemMatch: { $ne: user._id } }},
        {private: false},
        {rounds: {$size: 0}}
      ]
    });
  },
  rangePlayers(){
    return {
      min: 0,
      max: playerConstellations.length-1,
      value: playerConstellations.indexOf(nrPlayers.get()),
      nr: nrPlayers.get()
    };
  },
  rangeTeams(){
    let constellations = playerTeamsConstellations[nrPlayers.get()];
    return {
      min: 0,
      max: constellations.length-1,
      value: constellations.indexOf(nrTeams.get()),
      nr: nrTeams.get()
    };
  }
});
