import './rules.html';

Template.rules.onRendered(()=>{

});

Template.rules.events({

});

Template.rules.helpers({
  'cardAndValues'(){
    return [
      {symbol:  "A", name:    "Ass", value: "11"},
      {symbol: "10", name:   "Zehn", value: "10"},
      {symbol:  "K", name:  "König", value:  "4"},
      {symbol:  "O", name:   "Ober", value:  "3"},
      {symbol:  "U", name:  "Unter", value:  "2"},
      {symbol:  "7", name: "Sieben", value:  "0"},

    ]
  },
  'exclaimCards'(){
    return [
      {name:  "Binokel", value:  "40", dvalue:  "300", cards: [{color: "l",symbol: "O"},{color: "b",symbol: "U"}]},
      {name:    "4 Ass", value: "100", dvalue: "1000", cards: [{color: "a",symbol: "A"},{color: "l",symbol: "A"},{color: "h",symbol: "A"},{color: "b",symbol: "A"}]},
      {name: "4 Könige", value:  "80", dvalue: "1000", cards: [{color: "a",symbol: "K"},{color: "l",symbol: "K"},{color: "h",symbol: "K"},{color: "b",symbol: "K"}]},
      {name:   "4 Ober", value:  "60", dvalue: "1000", cards: [{color: "a",symbol: "O"},{color: "l",symbol: "O"},{color: "h",symbol: "O"},{color: "b",symbol: "O"}]},
      {name:  "4 Unter", value:  "40", dvalue: "1000", cards: [{color: "a",symbol: "U"},{color: "l",symbol: "U"},{color: "h",symbol: "U"},{color: "b",symbol: "U"}]},

      {name:         "Paar", value:  "20", dvalue: "40", cards: [{color: "a",symbol: "K"},{color: "a",symbol: "O"}]},
      {name:  "Trumpf Paar", value:  "40", dvalue: "80", cards: [{color: "a",symbol: "K"},{color: "a",symbol: "O"}]},

      {name:         "Familie", value:  "100", dvalue: "1500", cards: [{color: "a",symbol: "A"},{color: "a",symbol: "10"},{color: "a",symbol: "K"},{color: "a",symbol: "O"},{color: "a",symbol: "U"}]},
      {name:  "Trumpf Familie", value:  "150", dvalue: "1500", cards: [{color: "a",symbol: "A"},{color: "a",symbol: "10"},{color: "a",symbol: "K"},{color: "a",symbol: "O"},{color: "a",symbol: "U"}]},

      {name:  "Diss (Trumpf 7)", value:  "10", dvalue: "20", cards: [{color: "a",symbol: "7"}]},
    ]
  },
});
